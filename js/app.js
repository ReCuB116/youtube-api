function itemList(e, t) {
  res = e;
  for (var n = 0; n < t.length; n++) {
    res = res.replace(/\{\{(.*?)\}\}/g, function (e, r) {
      return t[n][r]
    })
  }
  return res
}

$(function () {
  $("form").on("submit", function (e) {
    e.preventDefault();
    var search = $("#search").val();
    var request = gapi.client.youtube.search.list({
      part: "snippet",
      type: "video",
      q: search,
      maxResults: 20,
      order: "viewCount",
      publishedAfter: "2015-01-01T00:00:00Z"
    });
    request.execute(function (response) {
      var results = response.result;
      console.log(results);
      $("#results").html("");
      $("#searchText").html('Результаты поиска - "<strong>' + search + '</strong>"');
      $.each(results.items, function (index, item) {
        $.get("tpl/item.html", function (data) {
          $("#results").append(itemList(data, [{
            "title": item.snippet.title,
            "videoid": item.id.videoId,
            "author": item.snippet.channelTitle,
            "date": item.snippet.publishedAt
          }]));
        });
      });
      resetVideoHeight();
    });
  });

  $(window).on("resize", resetVideoHeight);
});

function resetVideoHeight() {
  $(".video").css("height", $("#results").width() * 9 / 16);
}

function init() {
  gapi.client.setApiKey("AIzaSyD-GDB-BVE0rn_Eu3u2diCNtkhDsuJKbbg");
  gapi.client.load("youtube", "v3", function () {

  });
}

$(document).ready(function () {
  $('#results').on('click', 'h2[data-target]', function () {
    var id = $(this).attr('data-target');
    var height = $(id).find('.videoBlock').outerHeight();

    if ($(this).attr('data-open') != 'true') {
      $(this).attr({
        'data-open': 'true'
      });
      $(id).css({
        'height': height
      });
    } else {
      $(this).attr({
        'data-open': 'false'
      });
      $(id).css({
        'height': 0
      });
    }
  });
});